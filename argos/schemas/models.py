import traceback
from datetime import datetime
from typing import Literal

from pydantic import BaseModel

# XXX Refactor using SQLModel to avoid duplication of model data


class Task(BaseModel):
    id: int
    url: str
    domain: str
    check: str
    expected: str
    selected_at: datetime | None
    selected_by: str | None

    class Config:
        from_attributes = True

    def __str__(self):
        id = self.id
        url = self.url
        check = self.check
        return f"Task ({id}): {url} - {check}"


class SerializableException(BaseModel):
    error_message: str
    error_type: str
    error_details: str

    @staticmethod
    def from_exception(e: BaseException):
        return SerializableException(
            error_message=str(e),
            error_type=str(type(e).__name__),
            error_details=traceback.format_exc(),
        )


class AgentResult(BaseModel):
    task_id: int
    # The on-check status means that the service needs to finish the check
    # and will then determine the severity.
    status: Literal["success", "failure", "error", "on-check"]
    context: dict | SerializableException
