# Installing PostgreSQL

If you intend to use argos with Postgresql, you can install it with the following commands:

```bash
pip install -e ".[postgresql]"
```

Here are a few steps for you to install postgresql on your system:

## Debian

```bash
sudo apt install postgresql
```

By default, the `postgres` user has no password and can hence only connect if ran by the `postgres` system user. The following command will assign it:

```bash
sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'postgres';"
```

```bash
psql postgres -c "CREATE DATABASE argos;"
psql postgres -c "ALTER DATABASE argos SET TIMEZONE TO 'UTC';"
```

## MacOS

```bash
brew install postgresql@14
brew services start postgresql@14
createuser argos
createdb argos -O argos
```