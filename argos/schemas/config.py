from typing import Dict, List, Literal, Optional, Tuple

from pydantic import BaseModel, HttpUrl, field_validator
from pydantic.functional_validators import BeforeValidator
from typing_extensions import Annotated

from argos.schemas.utils import string_to_duration

# This file contains the pydantic schemas.
# For the database models, check in argos.server.models.

Severity = Literal["warning", "error", "critical"]


def parse_threshold(value):
    for duration_str, severity in value.items():
        days = string_to_duration(duration_str, "days")
        # Return here because it's one-item dicts.
        return (days, severity)


class SSL(BaseModel):
    thresholds: List[Annotated[Tuple[int, Severity], BeforeValidator(parse_threshold)]]


class WebsiteCheck(BaseModel):
    key: str
    value: str | List[str] | Dict[str, str]

    class Config:
        arbitrary_types_allowed = True

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        if isinstance(value, str):
            return {"expected": value}
        elif isinstance(value, dict):
            return value
        elif isinstance(value, list):
            return {"expected": value}
        else:
            raise ValueError("Invalid type")


def parse_checks(value):
    # To avoid circular imports
    from argos.checks import get_registered_checks

    available_names = get_registered_checks().keys()

    for name, expected in value.items():
        if name not in available_names:
            msg = f"Check should be one of f{available_names}. ({name} given)"
            raise ValueError(msg)
        if isinstance(expected, int):
            expected = str(expected)
        return (name, expected)


class WebsitePath(BaseModel):
    path: str
    checks: List[
        Annotated[
            Tuple[str, str],
            BeforeValidator(parse_checks),
        ]
    ]


class Website(BaseModel):
    domain: HttpUrl
    frequency: Optional[int] = None
    paths: List[WebsitePath]

    @field_validator("frequency", mode="before")
    def parse_frequency(cls, value):
        if value:
            return string_to_duration(value, "hours")


class Service(BaseModel):
    secrets: List[str]


class Alert(BaseModel):
    error: List[str]
    warning: List[str]
    alert: List[str]


class General(BaseModel):
    frequency: int
    alerts: Alert

    @field_validator("frequency", mode="before")
    def parse_frequency(cls, value):
        return string_to_duration(value, "minutes")


class Config(BaseModel):
    general: General
    service: Service
    ssl: SSL
    websites: List[Website]
