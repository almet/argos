# The HTTP API

Argos exposes a website and an API. The website is available at "/" and the API at "/api".

## Authentication

To access the API, you need to pass an authentication token in the `Authorization` header, as defined in the configuration file. It's a Bearer token, so you need to pass it as a header like this :

```
    "Authorization": "Bearer " + token
```

## Endpoints

You can also have access to the Swagger API documentation at http://<argos-url>/docs, and the ReDoc documentation at http://<argos-url>/redoc.