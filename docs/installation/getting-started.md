# Installation

So, you want to get started :-) Nothing complicated here, but here is a step by step guide.

## Requirements

- Python 3.11+
- PostgreSQL 13+

## Getting dependencies

Once you got the source locally, create a virtualenv and install the dependencies:

```bash
python3 -m venv venv
source venv/bin/activate
pip install -e .
```

## Configure

Prepare a configuration file, you can copy the `config-example.yaml` file and edit it:

```bash
cp config-example.yaml config.yaml
```

You can read more about the configuration in the [configuration section](configuration.md).

## Starting the server

You need to specify environment variables to configure the server, or you can put them in an `.env` file:

```{literalinclude} ../../.env.example
---
caption: .env
---
```

Then you can start the server:

```bash
argos server start
```

The server will read a `yaml` file at startup, and will populate the tasks specified in it. 

## Running the agent

And here is how to run the agent:

```bash
argos agent http://localhost:8000 "<auth-token>"
```

## Running cleaning tasks

You also need to run cleaning tasks periodically. `argos server clean --help` will give you more information on how to do that.

Here is a crontab example:

```bash
# Run the cleaning tasks every hour (at minute 7)
7 * * * * argos server clean --max-results 100000 --max-lock-seconds 3600
```
