# Argos monitoring

An HTTP monitoring service.

1. Define a list of websites to monitor
2. Specify a list of checks to run on these websites.
3. Argos will run the checks periodically and alert you if something goes wrong.

Internally, a HTTP API is exposed, and a job queue is used to distribute the checks to the agents.

- [Online documentation](http://framasoft.frama.io/framaspace/argos)
- [Issue tracker](https://framagit.org/framasoft/framaspace/argos/-/issues)

## Resquirements

- **Python**: 3.11+
- **Backends**: SQLite (development), PostgreSQL 14+ (production)

## Todo:

- [ ] Do not return empty list on / when no results from agents.
- [ ] donner un aperçu rapide de l’état de la supervision.
- [ ] Allow passing a dict to check
- [ ] Rename error in unexpected error
- [ ] Use background tasks for alerting
- [ ] Delete outdated tasks from config
- [ ] Implement alerting tasks
- [ ] Handles multiple alerting backends (email, sms, gotify)
- [ ] Un flag de configuration permet d’ajouter automatiquement un job de vérification de redirection 301 de la version HTTP vers HTTPS
- [ ] add an "unknown" severity for check errors
- [ ] Add a way to specify the severity of the alerts in the config
- [ ] Add a command to generate new authentication token