from argos.logging import logger


def handle_alert(config, result, task, severity):
    msg = f"task={task.id}, status={result.status}, {severity=}"
    logger.error(f"Alerting stub: {msg}")
