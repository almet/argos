# Command-line interface

<!-- [[[cog
    from argos.commands import cli
    from click.testing import CliRunner
    def help(args):
        title = "argos " + " ".join(args)
        cog.out("\n```man\n")
        result = CliRunner().invoke(cli, args)
        output = result.output.replace("Usage: cli ", "Usage: argos ")
        cog.out(output)
        cog.out("```\n\n")
 ]]] -->
<!-- [[[end]]] -->

## The argos cli
<!--
.. [[[cog
    help(["--help"])
.. ]]] -->

```man
Usage: argos [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  agent   Get and run tasks to the provided server.
  server
```

<!--[[[end]]]
-->

## Agent commands
<!--
.. [[[cog
    help(["agent", "--help"])
.. ]]] -->

```man
Usage: argos agent [OPTIONS] SERVER AUTH

  Get and run tasks to the provided server. Will wait for new tasks.

  Usage: argos agent https://argos.server "auth-token-here"

Options:
  --max-tasks INTEGER             Number of concurrent tasks this agent can run
  --wait-time INTEGER             Waiting time between two polls on the server
                                  (seconds)
  --log-level [DEBUG|INFO|WARNING|ERROR|CRITICAL]
  --help                          Show this message and exit.
```

<!--[[[end]]]
-->

## Server commands
<!--
.. [[[cog
    help(["server", "--help"])
.. ]]] -->

```man
Usage: argos server [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  clean  Clean the database (to run routinely)
  start  Starts the server.
```

<!--[[[end]]]
-->

### Server start
<!--
.. [[[cog
    help(["server", "start", "--help"])
.. ]]] -->

```man
Usage: argos server start [OPTIONS]

  Starts the server.

Options:
  --host TEXT        Host to bind
  --port INTEGER     Port to bind
  --reload           Enable hot reloading
  --log-config TEXT  Path to the logging configuration file
  --help             Show this message and exit.
```

<!--[[[end]]]
-->

### Server clean
<!--
.. [[[cog
    help(["server", "cleandb", "--help"])
.. ]]] -->

```man
Usage: argos server clean [OPTIONS]

  Clean the database (to run routinely)

  - Removes old results from the database. - Removes locks from tasks that have
  been locked for too long.

Options:
  --max-results INTEGER       Maximum number of results to keep
  --max-lock-seconds INTEGER  The number of seconds after which a lock is
                              considered stale
  --help                      Show this message and exit.
```

<!--[[[end]]]
-->