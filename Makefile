venv:
	python3 -m venv venv
install: venv
	venv/bin/pip install -e ".[dev,docs]"
docs: cog
	venv/bin/sphinx-build docs public
cog:
	venv/bin/cog -r docs/*.md
tests: install
	venv/bin/pytest
