# Argos monitoring

Argos is an HTTP monitoring service.

1. Define a list of websites to monitor
2. Specify a list of checks to run on these websites.
3. Argos will run the checks periodically and alert you if something goes wrong.

Internally, a HTTP API is exposed, and a job queue is used to distribute the checks on the network.

## Features

- **Made for large systems**: It's meant to supervise a large number of websites.
- **Server/Agent architecture**: The server is responsible for storing the configuration and the results of the checks. The agent is responsible for running the checks and sending the results to the server.
- **Extensible**: You can add new checks using pure python.
- **HTTP API**: You can use the HTTP API to get the results of the checks.

![Argos architecture](capture.png)
## Installation

```{code-block} bash
pip install argos-monitoring
argos server start
argos agent http://localhost:8000 "<auth-token>"
```

```{literalinclude} ../config-example.yaml
---
caption: config.yaml
---
```

## Next

::::{grid} 2
:::{grid-item-card}  Getting started
:link: installation/getting-started.html
The best way to get started with argos.
:::
:::{grid-item-card}  Developper docs
:link: /developer/overview.html
You want to know more about the internals ?.
:::
::::


```{toctree}
:caption: Getting started
:hidden:

installation/getting-started
installation/postgresql
cli
api
```

```{toctree}
:caption: Configuration
:hidden:
configuration
checks
```

```{toctree}
:caption: Developer docs
:hidden:
developer/overview
developer/new-check
developer/models

```
