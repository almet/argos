# Configuration

There are actually two configuration files: one for the service and one for the checks.

## Server configuration

The server configuration is done using environment variables. You can put them in a `.env` file at the root of the project.
Here is a list of the useful variables, in the `.env` format:

```{literalinclude} ../.env.example
---
caption: .env
---
```


## Checks configuration

Argos uses a YAML configuration file to define the websites to monitor and the checks to run on these websites.

Here is a simple configuration file:


```{literalinclude} ../config-example.yaml
---
caption: config.yaml
---

```