# The data model

```{mermaid}
classDiagram
direction RL
class Task {
    - url
    - domain
    - check
    - expected
    - frequency
    - selected_by
    - selected_at
    - completed_at
    - next_run
}
class Result{
    - task : Task
    - task_id
    - task
    - agent_id
    - submitted_at
    - status
    - severity
    - context
}
Result "*" o-- "1" Task : has many
```


```{literalinclude} ../../argos/server/models.py
---
caption: models.py
---
```